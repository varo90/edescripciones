<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('db_connections.php');
    include('queries.php');
    include('csv_files.php');
    
    /*****************/
    
    $db_my_desc = new db('my','edescriptions');
    $items_to_export = $db_my_desc->make_query(queries::get_non_exported_items());
    
    /*****************/

    if(sizeof($items_to_export) > 0) {
        $db_my_img = new db('my','imagenes_ecommerce');

        $route_file = 'downloaded/'.date("dmy").'_MASTERFILE.csv';
        $csv = new csv_files($route_file);
    
        $csv->write(get_csv_headers());
        $csv->write(get_csv_headers());
        $query = $db_my_desc->conn->prepare(queries::update_exported_item());
        $query2 = $db_my_img->conn->prepare(queries::update_im_description());
        foreach($items_to_export as $item) {
            $csv->write(get_row($item));
            $query->execute([1,$item->referencia,$item->color]);
            $query2->execute([$item->referencia,$item->color]);
        }
    
        unset($csv);
        unset($db_my_img);
        
        echo $route_file;
    }

    unset($db_my_desc);
    
    /////////////////////////////////////////////////////////////////////////////////////
        
    function get_row($product) {
        
        $item_data = array_fill(0, 39, '');

        $item_data[0] = $product->referencia . ' ' . $product->color;
        $item_data[1] = $product->permanent;
        $item_data[2] = $product->tallaje;
        $item_data[3] = $product->orientacion;
        $item_data[4] = utf8_decode($product->tit_esp);
        $item_data[5] = utf8_decode($product->tit_cat);
        $item_data[6] = utf8_decode($product->tit_eng);
        $item_data[8] = utf8_decode($product->desc_esp);
        $item_data[10] = $product->med_esp;
        $item_data[11] = $product->com_esp;
        $item_data[14] = utf8_decode($product->desc_cat);
        $item_data[16] = $product->med_cat;
        $item_data[17] = $product->com_cat;
        $item_data[20] = utf8_decode($product->desc_eng);
        $item_data[21] = $product->med_eng;
        $item_data[22] = $product->com_eng;
        $item_data[25] = $product->macro_cat_id;
        $item_data[26] = $product->sub_cat_id;
        $item_data[27] = $product->micro_color;
        $item_data[28] = substr($product->micro_color, 0, 2);
        $item_data[31] = $product->new_arrival;
        $item_data[35] = $product->on_off_line;
        $item_data[36] = $product->key_clients;
        $item_data[37] = $product->online_website;
        $item_data[38] = $product->rebajas;
        $item_data[39] = $product->add_cat_list;

        return $item_data;
    }
    
    function get_csv_headers() {
        $headers = ['SAP_MODEL_COLOR','SAP_PERMANENT Y/N','ID TABELLA CONVERSIONE TAGLIE','Verticale/orizzontale','TITOLO ARTICOLO LINGUA 1 (ESP)','TITOLO ARTICOLO LINGUA 2 (CAT)','TITOLO ARTICOLO LINGUA 3 (ENG)',
            'DESCRIZONE EDITORIALI LINGUA 1 (ESP)',
            'Material / Design, Pattern / Color ES',
            'Closure / Pockets / collar / cuffs ES',
            'Measures (ancho x alto x hondo) ES',
            'Composition ES',
            'Brand suggestions ES',
            'Fit CAT',
            'Material / Design, Pattern / Color CAT',
            'Closure / Pockets / collar / cuffs CAT',
            'Measures CAT',
            'Composition CAT',
            'Brand suggestions CAT',
            'Fit ENG',
            'Material / Design, Pattern / Color ENG',
            'Closure / Pockets / collar / cuffs ENG',
            'MeasureSize ENG',
            'Composition ENG',
            'Brand suggestions ENG',
            'Macro Categoria online ID',
            'Sub Categoria online ID',
            'Micro color',
            'Simple Color',
            'LOOK',
            'OCCASION',
            'new arrivals (Y/N)',
            'sku_crossell',
            'sku_related',
            'Sku_upsell',
            'online/offline/filter',
            'key clients',
            'online_website',
            'REBAJAS',
            'add_categorias_list'
        ];
        
        return $headers;
    }