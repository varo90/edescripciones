<?php
    include('header.php');
    include('get_offline_info.php');
?>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/bootstrap/js/jquery.min.js" type="text/javascript"></script>
<div class="contenedor">
    <table id="offline">
      <tr>
        <th>ID</th>
        <th>Nombre</th> 
        <th>Eliminar</th>
      </tr>
      <?php foreach($offline_brands as $brand) {
      ?>
        <tr>
          <td><?php echo $brand->id; ?></td>
          <td><?php echo $brand->name; ?></td> 
          <td><a class="btn btn-danger btn-sm delete-brand" role="button" aria-pressed="true">X</a></td>
        </tr>
      <?php
      } ?>
    </table>
    <div id="add-new-brand" class="row">
        <div class="col-xs-10">
            <select id="brand" name="categoria" class="form-control">
                <option value="">--- Selecciona una marca para añadir ---</option>
                <?php
                foreach($brands as $brand) {
                    $desc_brand = $brand->Code . ' - ' . $brand->Name;
                    $val = $brand->Code . '__' . $brand->Name;
                ?>
                    <option value="<?php echo $val; ?>"><?php echo $desc_brand; ?></option>
                <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div id="updated"></div>
</div>