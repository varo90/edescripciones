<?php
    include('session_init.php');
    $fich = basename($_SERVER['SCRIPT_FILENAME']);
    if ((empty($_SESSION['username_link']) || !isset($_SESSION['username_link'])) && $fich != 'login.php') {
        header("location:login.php");
    }
?>
<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Descripciones</title>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap-select.css">
    
    <link href="vendor/bootstrap/css/descriptions.css" rel="stylesheet">

    <script src="vendor/bootstrap/js/jquery.min.js"></script>

    <script src="vendor/bootstrap/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script src="vendor/bootstrap/js/bootstrap-select.min.js"></script>

    <script src="vendor/bootstrap/js/bootbox.min.js"></script>
    <!-- Custom styles for this template -->
    <!-- Sign in bootstrap -->
    <link href="vendor/bootstrap/css/signin.css" rel="stylesheet">
    <!-- Bootstrap -->
    <script type="text/javascript" charset="utf8" src="vendor/bootstrap/js/descriptions.js"></script>
    <!-- Elegant TextArea -->
    <!-- <script src="vendor/bootstrap/js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ 
        selector:'textarea',
        plugins: "paste",
//        menubar: "edit",
//        toolbar: "paste",
        paste_data_images: true
    });</script> -->

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                <a class="navbar-brand" href="../link/index.php"><img src='images/logo-se.png' alt='SE Logo'></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a class="navbar-brand" href="descriptions.php"><font color="<?php if($fich=="descriptions.php"){ echo 'red'; } ?>">Por hacer</font></a>
                </li>
                <li class="nav-item">
                    <!-- <a class="navbar-brand" href="descriptions_descripted.php"><font color="<?php //if($fich=="descriptions_descripted.php"){ echo 'red'; } ?>">Realizadas</font></a> -->
                    <a class="navbar-brand" href="../imagenes_ecom/descripciones_hechas.php" target="_blank"><font color="<?php if($fich=="descriptions_descripted.php"){ echo 'red'; } ?>">Realizadas</font></a>
                </li>
                <li class="nav-item">
                    <a class="navbar-brand" href="offline.php"><font color="<?php if($fich=="offline.php"){ echo 'red'; } ?>">Marcas Off-Line</font></a>
                </li>
                <li class="nav-item">
                    <a class="navbar-brand" href="microcolor.php"><font color="<?php if($fich=="microcolor.php"){ echo 'red'; } ?>">Colores</font></a>
                </li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <?php
                    include('session_init.php');
                    if(empty($_SESSION['username_link']) || !isset($_SESSION['username_link'])) { 
                ?>
                        <li class="nav-item">
                          <a class="nav-link" href="login.php">Acceder</a>
                        </li>
                <?php
                }
                else { ?>
                        <li class="nav-item">
                          <a class="nav-link" href=""><?php echo $_SESSION['username_link']; ?></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="logout.php">Salir</a>
                        </li>
                <?php
                }
                ?>
              </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>