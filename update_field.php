<?php

    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    include('db_connections.php');
    include('queries.php');

    $field = $_POST['field'];
    $ref = $_POST['ref'];
    $value = $_POST['value'];
    $color = $_POST['color'];

    $my_conn = new db('my','edescriptions');

    $query = $my_conn->conn->prepare(queries::update_field($field));
    $query->execute([$value,$ref,$color]);

    if($field == 'micro_color') {
        $simple_color = substr($value, 0, 2);
        $query = $my_conn->conn->prepare(queries::update_field('simple_color'));
        $query->execute([$simple_color,$ref,$color]);
    }

    if($field == 'exported') {
        $query = $my_conn->conn->prepare(queries::update_exported_item());
        $query->execute([$value,$ref,$color]);

        $my_conn2 = new db('my','imagenes_ecommerce');
        $query = $my_conn2->conn->prepare(queries::update_descrito_item_imagenes());
        $query->execute([0,$ref,$color]);
        unset($my_conn2);
    }

    unset($my_conn);