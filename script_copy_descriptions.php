<?php

    error_reporting(E_ALL);
    ini_set('display_errors',  'On');
    include('db_connections.php');
    include('queries.php');

    $db_my = new db('my', 'edescriptions');
    $uploaded = $db_my->make_query(queries::get_uploaded());

    $qmarks_array = array();
    $qmarks_array = array_pad ( $qmarks_array , 45 , '?' );
    $qmarks = implode(',',$qmarks_array);
    $query = $db_my->conn->prepare(queries::insert_descripcion($qmarks));
    //$query = $db_my->conn->prepare(queries::update_field('exported'));
    foreach($uploaded as $up) {
        $reference = explode(' ', $up->model_color);
        $ref = $reference[0];
        $col = $reference[1];
        $query->execute([$ref, $col, $up->permanent, $up->tallaje, $up->orientacion, $up->tit_esp, $up->tit_cat, $up->tit_eng, 
                        $up->fit_esp, $up->desc_esp, $up->closure_esp, $up->med_esp, $up->com_esp, $up->brand_sug_esp, $up->fit_cat, 
                        $up->desc_cat, $up->closure_cat, $up->med_cat, $up->com_cat, $up->brand_sug_cat, $up->fit_eng, $up->desc_eng, 
                        $up->closure_eng, $up->med_eng, $up->com_eng, $up->brand_sug_eng, $up->macro_cat_id, $up->sub_cat_id, 
                        $up->micro_color, $up->simple_color, $up->look, $up->occcasion, $up->new_arrival, 
                        $up->sku_crossell, $up->sku_related, $up->sku_upsell, $up->on_off_line, $up->key_clients, $up->online_website, 
                        $up->rebajas, $up->add_cat_list, $up->exported, $up->loaded, $up->loaded, $up->loaded]);
        //$query->execute([1,$ref,$col]);
    }

    unset($db_my);