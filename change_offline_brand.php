<?php
    include('db_connections.php');
    include('queries.php');
    
    $brand = explode('__',$_POST['id']);
    $id = $brand[0];
    $desc = $brand[1];
    
    /****************/
    
    $queries = new queries();
    
    if($_POST['role'] == 'add') {
        $sql = $queries->add_offline_brand();
        $params = [$id,$desc];
    } else {
        $sql = $queries->del_offline_brand();
        $params = [$id];
    }
    
    /****************/
    
    $db_my = new db('my','edescriptions');
    
    $query = $db_my->conn->prepare($sql);
    $query->execute($params);

    unset($db_my);