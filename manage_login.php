<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include('db_connections.php');
include('session_init.php');

$conn = new db('my','employees');
// Prepare query and bind variables
$query = $conn->conn->prepare("SELECT * FROM employees WHERE email_se=:session_name AND itickets_password=:pass");
$query->bindParam(':session_name', $email, PDO::PARAM_STR);
$query->bindParam(':pass', $pass, PDO::PARAM_STR);

$user_name = strtolower($_POST['UserName']);
$email = $user_name . '@santaeulalia.com';
$pass = $_POST['Password'];

try {
    if($query->execute()) {
        echo 'User: ' . $user_name . '. Pass: ' . $pass;
        if($query->rowCount() > 0) {
            $result = $query->fetch(PDO::FETCH_OBJ);
            $fullname = explode(' ',$result->full_name);
            $nom = get_nom($fullname);
            $_SESSION['fullname_link'] = $nom;
            $_SESSION['username_link'] = $user_name;
            $_SESSION['userid_link'] = $result->id;
            $_SESSION['usergroup_link'] = $result->id_group;
            $_SESSION['userdpto_link'] = $result->department;
            echo $_SESSION['username_link'];
            echo '<br>';
            echo $_SESSION['userid_link'];
            echo '<br>';
            echo $_SESSION['usergroup_link'];
            echo '<br>';
            echo $_SESSION['userdpto_link'];
        }
        unset($conn);
        header("location:index.php");
    }
}
catch (PDOException $e) {
    echo $e->getMessage() . '<br>';
}

unset($conn);

function get_nom($fullname) {
    if(sizeof($fullname) == 1) {
        $nom = $fullname[0];
    } else if(sizeof($fullname) >=2 && sizeof($fullname) <= 3) {
        $nom = $fullname[0] . ' ' . $fullname[1];
    } else {
        $nom = $fullname[0] . ' ' . $fullname[1] . ' ' . $fullname[2];
    }
    return $nom;
}