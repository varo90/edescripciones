<?php
    include('header.php');
    include('get_microcolor.php');
?>
<div class="contenedor">
    <table class="table_edit table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Desc. Esp.</th> 
                <th>Desc. Cat.</th>
                <th>Desc. Eng.</th>
                <th>Label Esp.</th>
                <th>Label Cat.</th>
                <th>Label Eng.</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($microcolors as $microcolor) {
            ?>
            <tr>
                <td><?php echo $microcolor->sap_color_id; ?></td>
                <td><?php echo $microcolor->desc_esp; ?></td> 
                <td><?php echo $microcolor->desc_cat; ?></td> 
                <td><?php echo $microcolor->desc_ing; ?></td> 
                <td><?php echo $microcolor->label_esp; ?></td> 
                <td><?php echo $microcolor->label_esp; ?></td> 
                <td><?php echo $microcolor->label_esp; ?></td> 
                <td><a class="btn btn-danger btn-sm delete-microcolor" role="button" aria-pressed="true">X</a></td>
            </tr>
            <?php
            } ?>
        </tbody>
    </table>
    <div id="add-new-microcolor" class="row">
        <div class="col-sm-1">
            <input id="microcolor_add_id" type="number" class="form-control microcolor_add" placeholder="ID">
        </div>
        <div class="col-sm-2">
            <input id="microcolor_add_esp" type="text" class="form-control microcolor_add" placeholder="Nombre Esp">
        </div>
        <div class="col-sm-2">
            <input id="microcolor_add_cat" type="text" class="form-control microcolor_add" placeholder="Nombre Cat">
        </div>
        <div class="col-sm-2">
            <input id="microcolor_add_eng" type="text" class="form-control microcolor_add" placeholder="Nombre Eng">
        </div>
        <div class="col-sm-1">
            <input id="microcolor_add_lb_esp" type="text" class="form-control microcolor_add" placeholder="Label Esp">
        </div>
        <div class="col-sm-1">
            <input id="microcolor_add_lb_cat" type="text" class="form-control microcolor_add" placeholder="Label Cat">
        </div>
        <div class="col-sm-1">
            <input id="microcolor_add_lb_eng" type="text" class="form-control microcolor_add" placeholder="Label Eng">
        </div>
        <div class="col-sm-2">
            <a id='add_microcolor' class="btn btn-info btn-lg microcolor_add" role="button" aria-pressed="true">Añadir</a>
        </div>
    </div>
    <div id="updated"></div>
</div>