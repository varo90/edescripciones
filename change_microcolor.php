<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('db_connections.php');
    include('queries.php');
    
    /****************/
    
    $queries = new queries();
    
    if($_POST['role'] == 'add') {
        $sql = $queries->add_microcolor();
        $simple_color = substr($_POST['id'], 0, 2);
        $params = [$_POST['id'],$_POST['desc_esp'],$_POST['desc_cat'],$_POST['desc_eng'],$simple_color,$_POST['label_esp'],$_POST['label_cat'],$_POST['label_eng']];
    } else {
        $sql = $queries->del_microcolor();
        $params = [$_POST['id']];
    }
    
    /****************/
    
    $db_my = new db('my','edescriptions');
    
    $query = $db_my->conn->prepare($sql);
    $query->execute($params);

    unset($db_my);