<?php

class queries {
    
    static function get_mysql_items() {
        $sql = "SELECT * FROM imagenes WHERE subido=1 AND descripcion=0 ORDER BY fecha_subido";
        return $sql;
    }
    
    static function get_mysql_items_descripted() {
        $sql = "SELECT * FROM imagenes WHERE subido=1 AND descripcion=1 ORDER BY fecha_subido DESC";
        return $sql;
    }
    
    static function get_uploaded() {
        $sql = "SELECT * FROM uploaded";
        return $sql;
    }
    
    static function insert_descripcion($qmarks) {
        $sql = "INSERT IGNORE INTO descripciones VALUES ($qmarks)";
        return $sql;
    }
    
    static function get_table_data($table) {
        $sql = "SELECT * FROM $table ORDER BY 1";
        return $sql;
    }
    
    static function get_item_data() {
        $sql = "SELECT * FROM imagenes WHERE referencia=? AND color=?";
        return $sql;
    }
    
    static function get_brands() {
        $sql = 'SELECT FirmCode as Code, FirmName COLLATE Modern_Spanish_CI_AS as Name FROM dbo.OMRC with (NOLOCK) WHERE FirmCode > -1';
        return $sql;
    }
    
    static function insert_edescription($columns,$qmarks) {
        $sql = "INSERT INTO uploaded ($columns) VALUES ($qmarks)";
        return $sql;
    }
    
    static function get_db_columns($table) {
        $sql = "DESCRIBE uploaded";
        return $sql;
    }
    
    static function update_im_description() {
        $sql = "UPDATE imagenes SET descripcion=1, fecha_descripcion=NOW() WHERE referencia=? AND color=?";
        return $sql;
    }
    
    static function check_marca_offline() {
        $sql = "SELECT COUNT(*) AS 'total' FROM marcas_offline WHERE id=? LIMIT 1";
        return $sql;
    }
    
    static function add_offline_brand() {
        $sql = "INSERT INTO marcas_offline (id,name) VALUES (?,?)";
        return $sql;
    }
    
    static function del_offline_brand() {
        $sql = "DELETE FROM marcas_offline WHERE id=?";
        return $sql;
    }
    
    static function add_microcolor() {
        $sql = "INSERT INTO colors (sap_color_id,desc_esp,desc_cat,desc_ing,simple_color,label_esp,label_cat,label_ing) VALUES (?,?,?,?,?,?,?,?)";
        return $sql;
    }
    
    static function del_microcolor() {
        $sql = "DELETE FROM colors WHERE sap_color_id=?";
        return $sql;
    }

    static function create_row() {
        $sql = "INSERT IGNORE INTO descripciones(referencia,color,on_off_line,add_cat_list,loaded) VALUES(?,?,?,?,NOW())";
        return $sql;
    }

    static function clone_row() {
        $sql = "CREATE TEMPORARY TABLE desc2 ENGINE=MEMORY SELECT * FROM descripciones WHERE referencia=? AND color=?;
                UPDATE desc2 SET color=?;
                INSERT INTO descripciones SELECT * FROM desc2;
                DROP TABLE desc2;";
        return $sql;
    }

    static function update_field($field) {
        $sql = "UPDATE descripciones SET $field=? WHERE referencia=? AND color=?";
        return $sql;
    }

    static function get_non_exported_items() {
        $sql = "SELECT * FROM descripciones WHERE exported = 0";
        return $sql;
    }

    static function update_exported_item() {
        $sql = "UPDATE descripciones SET exported=?, date_exported=NOW() WHERE referencia=? AND color=?";
        return $sql;
    }

    static function update_descrito_item_imagenes() {
        $sql = "UPDATE imagenes SET descripcion=?, fecha_descripcion=NOW() WHERE referencia=? AND color=?";
        return $sql;
    }

    static function get_item_description() {
        $sql = "SELECT * FROM descripciones WHERE referencia=? AND color=?";
        return $sql;
    }

    static function get_similar_item() {
        $sql = "SELECT * FROM descripciones WHERE referencia=?";
        return $sql;
    }

    static function get_info_proveedor() {
        $sql = "SELECT CONVERT(VARCHAR, oi.UserText)COLLATE Modern_Spanish_CI_AS as comentarios,
                    CASE WHEN tcmca.U_GSP_MATERIALDESC IS NULL THEN '' ELSE CONCAT(' - ',tcmca.U_GSP_MATERIALDESC) END AS material,
                    CASE WHEN tcmca.U_GSP_COLORDESC IS NULL THEN '' ELSE CONCAT('',tcmca.U_GSP_COLORDESC) END AS color
                FROM [dbo].[@GSP_TCMODELCATALOG] tcmca WITH (NOLOCK)
                LEFT JOIN OITM oi WITH (NOLOCK) ON oi.U_GSP_MODELCODE = tcmca.U_GSP_MODELCODE
                LEFT JOIN dbo.[@GSP_TCCOLOR] tc WITH (NOLOCK) ON tc.Code = oi.U_GSP_Color
                WHERE oi.U_GSP_REFERENCE=? AND tc.U_GSP_Name=?";
        return $sql;
    }
    
    function join_conditions($filters,$sql_ref,$sql_col) {
        $texts = array();
        foreach($filters as $filter) {
            if($filter == null) {continue;}
            $texts[] = "($sql_ref='$filter->reference' AND $sql_col='$filter->color')";
        }
        $conditions = implode(' OR ', $texts);
        return $conditions;
    }
    
//     function get_to_do_desc() {
//         $cond_uploaded = get_uploaded_items();
        
//         $sql = "SELECT DISTINCT TOP 200 tcm.U_GSP_Picture as picture, tcm.U_GSP_Desc collate Modern_Spanish_CI_AS as descr,
//                        CONCAT(tcm.U_GSP_REFERENCE, CASE WHEN tcmco.U_GSP_NAME IS NULL THEN '' ELSE CONCAT(' ',tcmco.U_GSP_NAME) END) AS id_sap,
//                        tcm.U_GSP_FirmCode as id_marca,
//                        tcm.U_GSP_SeasonCode as season
//                 FROM OITM oi with (NOLOCK)
//                     LEFT JOIN [dbo].[@GSP_TCMODEL] tcm with (NOLOCK) ON oi.U_GSP_REFERENCE = tcm.U_GSP_REFERENCE
//                     LEFT JOIN [dbo].[@GSP_TCCOLOR] tcmco with (NOLOCK) ON oi.U_GSP_COLOR = tcmco.Code
//                     LEFT JOIN [dbo].[@GSP_TCMODELCATALOG] tcmca with (NOLOCK) ON tcm.Code = tcmca.U_GSP_MODELCODE and tcmca.U_GSP_MODELCOLOR = oi.U_GSP_COLOR
//                 WHERE LEFT(tcmca.U_GSP_ColorDesc,1)='@' AND $cond_uploaded";
//     }
}

// function get_uploaded_items() {
//     $db='edescriptions';
//     $conn = new db(['my',$db]);
    
//     $sql = 'SELECT model_color FROM uploaded';
//     $gsent = $conn->prepare($sql);
//     $gsent->execute();
//     $uploadeds = $gsent->fetchAll();
    
//     $ids_sap = array();
//     foreach($uploadeds as $uploaded) {
//         $id = $uploaded['model_color'];
//         $ids_sap[] = "CONCAT(tcm.U_GSP_REFERENCE, CASE WHEN tcmco.U_GSP_NAME IS NULL THEN '' ELSE CONCAT(' ',tcmco.U_GSP_NAME) END) <> '$id'";
//     }
//     $str_ids_sap = implode(' AND ', $ids_sap);
    
//     unset($conn);
    
//     return $str_ids_sap;
// }
