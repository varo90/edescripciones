<?php
    include('header.php');
    include('get_tallajes_info.php');
?>
<div class="contenedor">
    <table id="tallajes">
      <tr>
        <th>id_size</th>
        <th>category</th> 
        <th>sz_1</th>
        <th>sz_2</th>
        <th>sz_3</th>
        <th>sz_4</th>
        <th>sz_5</th>
        <th>sz_6</th>
        <th>sz_7</th>
        <th>sz_8</th>
        <th>sz_9</th>
        <th>sz_10</th>
        <th>sz_11</th>
        <th>sz_12</th>
        <th>sz_13</th>
        <th>sz_14</th>
        <th>sz_15</th>
        <th>sz_16</th>
        <th>sz_17</th>
      </tr>
      <?php foreach($tallajes as $tallaje) {
      ?>
        <tr>
          <td><?php echo $tallaje->id_size; ?></td>
          <td><?php echo $tallaje->category; ?></td> 
          <td><?php echo $tallaje->sz_1; ?></td>
          <td><?php echo $tallaje->sz_2; ?></td> 
          <td><?php echo $tallaje->sz_3; ?></td>
          <td><?php echo $tallaje->sz_4; ?></td> 
          <td><?php echo $tallaje->sz_5; ?></td>
          <td><?php echo $tallaje->sz_6; ?></td> 
          <td><?php echo $tallaje->sz_7; ?></td>
          <td><?php echo $tallaje->sz_8; ?></td> 
          <td><?php echo $tallaje->sz_9; ?></td>
          <td><?php echo $tallaje->sz_10; ?></td> 
          <td><?php echo $tallaje->sz_11; ?></td>
          <td><?php echo $tallaje->sz_12; ?></td> 
          <td><?php echo $tallaje->sz_13; ?></td>
          <td><?php echo $tallaje->sz_14; ?></td> 
          <td><?php echo $tallaje->sz_15; ?></td>
          <td><?php echo $tallaje->sz_16; ?></td> 
          <td><?php echo $tallaje->sz_17; ?></td> 
          
        </tr>
      <?php
      } ?>
    </table>
    <div id="updated"></div>
</div>