// Initialize tooltip component
/*$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// Initialize popover component
$(function () {
  $('[data-toggle="popover"]').popover()
})*/

var id;
var field;
var value;
var ref;
var color;
    
function save_data(e) {
    get_fields(e);
    update_field();
}

function get_fields(e) {
    id = $(e).attr('id');
    var text = id.split('__');
    var id_sap = text[text.length-1];
    var text2 = id_sap.split('_');
    field = text[0];
    value = $(e).val();
    ref = text2[text2.length-2];
    color = text2[text2.length-1];
}

function update_field() {
    $.ajax({
        url: 'update_field.php',
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        data: {field:field,value:value,ref:ref,color:color},
        success: function( data ){
            $('#pechos_'+ref+'_'+color).html( data );
            console.log(field + ': ' + value);
        }
    });
}

function reloading() {
    location.reload();
}

$( document ).ready(function() {
    $('.row_desc').on( 'click', function () {
        id = $(this).attr('id');
        refcolor = $(this).parent().find(':nth-child(2)').html();
        $.get("get_form.php", {id:id,refcolor:refcolor},function(data,status){
            bootbox.dialog({
                message: data,
                title: "<strong><center>"+refcolor+"</center></strong>",
                buttons: {
                    success: {
                        label: "Close",
                        className: "btn-primary",
                        callback: reloading
                    }
                },
                onEscape: function() {
                    reloading();
                }
            }).off("shown.bs.modal");
        });
    });
    
    $(".selectpicker").selectpicker({
        "title": "Select Options"        
    }).selectpicker("render");

    $('#send_descriptions').on( 'click', function (e) {
        $.ajax({
            url: 'generate_masterfile.php',
            type: 'post',
            datatype: 'json',
            data: {},
            success: function( data ){
                download(data,e);
                // goindex();
            }
        });
    } );
    
    $('.delete-brand').on( 'click', function (e) {
        var id = $(this).parent().parent().find(':nth-child(1)').html();
        var args = {id:id,role:'del'};
    	modify(args,'change_offline_brand.php');
    });
    
    $('#brand').on( 'change', function (e) {
        var id = $('#brand').find(":selected").val();
        var args = {id:id,role:'add'};
    	modify(args,'change_offline_brand.php');
    });

    $('#add_microcolor').on( 'click', function () {
        var id = $('#microcolor_add_id').val();
        var desc_esp = $('#microcolor_add_esp').val();
        var desc_cat = $('#microcolor_add_cat').val();
        var desc_eng = $('#microcolor_add_eng').val();
        var label_esp = $('#microcolor_add_lb_esp').val();
        var label_cat = $('#microcolor_add_lb_cat').val();
        var label_eng = $('#microcolor_add_lb_eng').val();
        var args = {id:id,desc_esp:desc_esp,desc_cat:desc_cat,desc_eng:desc_eng,label_esp:label_esp,label_cat:label_cat,label_eng:label_eng,role:'add'};
        modify(args,'change_microcolor.php');
    });

    $('.delete-microcolor').on( 'click', function () {
        var id = $(this).parent().parent().find(':nth-child(1)').html();
        var args = {id:id,role:'del'};
    	modify(args,'change_microcolor.php');
    });
    
    function modify(args,page) {
        $.ajax({
            url: page,
            type: 'post',
            //dataType: 'application/x-www-form-urlencoded',
            data: args,
            success: function( data ){
                reloading();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                // alert("Status: " + textStatus); alert("Error: " + errorThrown); 
            }       
        });
    }
    
    function download(file,e) {
        e.preventDefault();  //stop the browser from following
        $(location).attr('href',file);
        $('#updated').html('<font color="green">Hecho.</font>');
    }
    function goindex() {
        window.location = 'index.php';
    }
});
