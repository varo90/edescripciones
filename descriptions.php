<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'Off');
    include('header.php');
    include('db_connections.php');
    include('queries.php');
    include('session_init.php');

    $db_my = new db('my','imagenes_ecommerce');
    $items = $db_my->make_query(queries::get_mysql_items(),[]);
    unset($db_my);
?>
<!--Latest compiled and minified Bootstrap JavaScript -->
<div class="contenedor">
    <div class="container-fluid">
        <div id="products" role="tablist" aria-multiselectable="true">
            <h3>Descripciones pendientes: <?php echo count($items); ?></h3>
            <div id="tabla_pendientes">
                <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Imagen</th>
                        <th scope="col">Referencia</th>
                        <th scope="col">Descripci&oacute;n</th>
                        <th scope="col">Temporada</th>
                        <th scope="col">Continuidad</th>
                        <th scope="col">Con fotos E.C.</th>
                        <th scope="col">Fecha subido</th>
                        <th scope="col">Por exportar</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $my_conn = new db('my','edescriptions');
                $query = $my_conn->conn->prepare(queries::get_item_description());
                foreach($items as $count => $item) {
                    $season = explode('\\', $item->imagen_sap)[0];
                    $img = "../../Fotos_Sap/$item->imagen_sap";
                    $foto_ec = $item->foto == 1 ? '<font color="green">Con fotos E.C.</font>' : '<font color="red">Sin fotos E.C.</font>';
                    $hoy = new DateTime(date('Y-m-d'));
                    $subido = new DateTime($item->fecha_subido);
                    $diferencia_fechas = $hoy->diff($subido);
                    $fecha_subido = date('d/m/Y h:i:s', strtotime($item->fecha_subido));
                    $fecha_subido = $diferencia_fechas->days < 5 ? $fecha_subido : '<font color="red">' .$fecha_subido. '</font>';

                    $query->execute([$item->referencia,$item->color]);
                    $item_desc = $query->fetch(PDO::FETCH_OBJ);

                    $class_border_color = '';
                    if($item_desc != null) {
                        if($item_desc->exported == 0) {
                            $selected0 = '';
                            $selected1 = 'selected="selected"';
                            $class_border_color = 'green-border';
                        } else {
                            $selected0 = 'selected="selected"';
                            $selected1 = '';
                        }
                    } else {
                        $selected0 = 'selected="selected"';
                        $selected1 = '';
                    }

                    $continuidad = $item->continuidad == 1 ? 'Continuidad' : '';
                ?>
                    <tr id="<?php echo $item->id ?>">
                        <th class="row_desc"><img src='<?php echo $img ?>' height='120' width='120'></th>
                        <td class="row_desc"><?php echo $item->referencia . ' ' . $item->color; ?></td>
                        <td class="row_desc"><?php echo $item->nombre ?></td>
                        <td class="row_desc"><?php echo $season ?></td>
                        <td class="row_desc"><?php echo $continuidad ?></td>
                        <td class="row_desc"><?php echo $foto_ec ?></td>
                        <td class="row_desc"><?php echo $fecha_subido ?></td>
                        <td>
                            <select id="exported__<?php echo $item->referencia . '_' . $item->color; ?>" name="exported" class="form-control <?php echo $class_border_color ?>" onchange="save_data(this)">
                                <option value="1" <?php echo $selected0; ?>><font color="red">No exportar</font></option>
                                <option value="0" <?php echo $selected1; ?>><font color="green">Exportar</font></option>
                            </select>
                        </tr>
                <?php
                }
                unset($my_conn);
                ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <br>
    <div id="updated"></div>
    <center>
        <a id='send_descriptions' class="btn btn-info btn-lg" role="button" aria-pressed="true">Exportar descripciones</a>
    </center>
</div>
