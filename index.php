<?php
    include('session_init.php');
    include('db_connections.php');
    if (empty($_SESSION['username_link']) || !isset($_SESSION['username_link'])) {
        header("location:login.php");
    }
    header("location:descriptions.php");