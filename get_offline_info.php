<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    
    include('db_connections.php');
    include('queries.php');
    
    /*****************/
    
    $queries = new queries();
    $db_my = new db('my','edescriptions');
    
    $offline_brands = $db_my->make_query($queries->get_table_data('marcas_offline'));
    
    unset($db_my);
    
    /*****************/
    
    $db_ms = new db();
    
    $brands = $db_ms->make_query($queries->get_brands());
    
    unset($db_ms);