<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    
    include('db_connections.php');
    include('queries.php');
    
    /*****************/
    
    $queries = new queries();
    $db_my = new db('my','edescriptions');
    
    $tallajes = $db_my->make_query($queries->get_table_data('tallajes'));
    
    unset($db_my);