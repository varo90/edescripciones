<?php
class csv_files {
    var $fp;
    
    function __construct($route_file) {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.basename($route_file));
        $this->fp = fopen($route_file, 'w');
    }
    
    function write($row) {
        fputcsv($this->fp,$row,';');
    }
    
    function __destruct() {
        fclose($this->fp);
    }
}