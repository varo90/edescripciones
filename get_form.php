<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('db_connections.php');
    include('queries.php');
    
    $refcolor = explode(' ',$_GET['refcolor']);
    $ref = $refcolor[0];
    $color = $refcolor[1];
    $id_sap = str_replace(' ','_',$_GET['refcolor']);

    $db_my = new db('my','imagenes_ecommerce');
    $item = $db_my->make_query(queries::get_item_data(),[$ref,$color])[0];
    unset($db_my);

    $db_my = new db('my','edescriptions');
    $item_desc = $db_my->make_query(queries::get_item_description(),[$ref,$color]);
    unset($db_my);
    
    include('get_fields_info.php');

    if(empty($item_desc) && $copia == 0) {
        $words_description = explode(' ',$item->nombre);
        if(strlen(end($words_description)) == 0) {
            unset($words_description[sizeof($words_description)-1]);
        }
        $brand = end($words_description);
        $on_off_line = get_on_off_line($brand);
        $gender = ($ref == 3 || $ref == 4 || $ref == 5 || $ref == 6) ? '1385' : '1384';

        $my_conn = new db('my','edescriptions');
        $query = $my_conn->conn->prepare(queries::create_row());
        $query->execute([$ref,$color,$on_off_line,$gender]);
        unset($my_conn);
    }

    $db_ms = new db();
    $info_proveedor = $db_ms->make_query(queries::get_info_proveedor(),[$ref,$color])[0];
    unset($db_ms);

    $img = "../../Fotos_Sap/$item->imagen_sap";
?>   

    <img src='<?php echo $img ?>' width="100%"><br>
    <font><center>
        <b><?php echo $item->nombre; ?></b><br>
        <?php echo 'Comentarios: <b>'.$info_proveedor->comentarios.'</b>'; ?><br>
        <?php echo 'Material: <b>'.$info_proveedor->material.'</b>'; ?><br>
        <?php echo 'Color proveedor: <b>'.$info_proveedor->color.'</b>'; ?>
    </center></font>
    <div id="pechos_<?php echo $ref.'_'.$color; ?>"></div>

    <form method="POST">
        <div id="tit_<?php echo $id_sap; ?>" class="npt titulo text-center">
            
        </div>

        <div class="npt titulo">
            <input id="tit_esp__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $tit_esp ?>" placeholder="T&iacute;tulo en castellano" onblur="save_data(this)" >
            <input id="tit_cat__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $tit_cat ?>" placeholder="T&iacute;tulo en catal&aacute;n" onblur="save_data(this)" >
            <input id="tit_eng__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $tit_eng ?>" placeholder="T&iacute;tulo en ingl&eacute;s" onblur="save_data(this)" >
        </div>

        <div class="npt descripcion">
            <label for="desc_esp__<?php echo $id_sap; ?>">Descripci&oacute;n en castellano:</label>
            <textarea id="desc_esp__<?php echo $id_sap; ?>" class="form-control" rows="10" onblur="save_data(this)"><?php echo $desc_esp ?></textarea>
            <label for="desc_cat__<?php echo $id_sap; ?>">Descripci&oacute;n en catal&aacute;n:</label>
            <textarea id="desc_cat__<?php echo $id_sap; ?>" class="form-control" rows="10" onblur="save_data(this)"><?php echo $desc_cat ?></textarea>
            <label for="desc_eng__<?php echo $id_sap; ?>">Descripci&oacute;n en ingl&eacute;s:</label>
            <textarea id="desc_eng__<?php echo $id_sap; ?>" class="form-control" rows="10" onblur="save_data(this)"><?php echo $desc_eng ?></textarea>
        </div>

        <div class="npt medida">
            <input id="med_esp__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $med_esp ?>" placeholder="Medidas en castellano" onblur="save_data(this)">
            <input id="med_cat__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $med_cat ?>" placeholder="Medidas en catal&aacute;n" onblur="save_data(this)">
            <input id="med_eng__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $med_eng ?>" placeholder="Medidas en ingl&eacute;s" onblur="save_data(this)">
        </div>

        <div class="npt composicion">
            <input id="com_esp__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $com_esp ?>" placeholder="Composici&oacute;n en castellano" onblur="save_data(this)">
            <input id="com_cat__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $com_cat ?>" placeholder="Composici&oacute;n en catal&aacute;n" onblur="save_data(this)">
            <input id="com_eng__<?php echo $id_sap; ?>" type="text" class="form-control" value="<?php echo $com_eng ?>" placeholder="Composici&oacute;n en ingl&eacute;s" onblur="save_data(this)">
        </div>

        <?php
        if($orientacion == 'verticale') {
            $selected_v = 'selected="selected"';
            $selected_h = '';
        } else {
            $selected_v = '';
            $selected_h = 'selected="selected"';
        }
        ?>
        <div class="npt orientacion-foto">
            <select id="orientacion__<?php echo $id_sap; ?>" name="orientacion" class="form-control" onchange="save_data(this)">
                <option value="verticale" <?php echo $selected_v ?>>Orientaci&oacute;n Vertical</option>
                <option value="orizzontale" <?php echo $selected_h ?>>Orientaci&oacute;n Horizontal</option>
            </select>
        </div>

        <div class="npt tallaje">
            <select id="tallaje__<?php echo $id_sap; ?>" name="tallaje" class="form-control" onchange="save_data(this)">
                <option value="">--- Elije un tallaje ---</option>
            <?php
            foreach($tallajes as $tallaje) {
                $size = $tallaje->id_size;
                if($size == $tallaj) {
                    $selected = 'selected="selected"';
                } else {
                    $selected = '';
                }
            ?>
                <option value="<?php echo $size; ?>" <?php echo $selected ?>><?php echo $size; ?></option>
            <?php
            }
            ?>
            </select>
        </div>
        <a class="subpie" href="tallajes.php" target="_blank">Ver tabla de tallajes</a>

        <div class="npt macro-cat">
            <select id="macro_cat_id__<?php echo $id_sap; ?>" name="categoria" class="form-control" onchange="save_data(this)">
                <option value="">--- Elije una Macro-Categor&iacute;a ---</option>
                <?php
                foreach($categorias as $categoria) {
                    $id_cat = $categoria->id;
                    $desc_cat = $id_cat . ' - ' . $categoria->nom_en . ' / ' . $categoria->nom_es;
                    if($id_cat == $macro_cat_id) {
                        $selected = 'selected="selected"';
                    } else {
                        $selected = '';
                    }
                ?>
                    <option value="<?php echo $id_cat; ?>" <?php echo $selected ?>><?php echo $desc_cat; ?></option>
                <?php
                }
                ?>
            </select>
        </div>

        <div class="npt micro-cat">
            <select id="sub_cat_id__<?php echo $id_sap; ?>" name="categoria" class="form-control" onchange="save_data(this)">
                <option value="">--- Elije una Subategor&iacute;a ---</option>
                <?php
                foreach($categorias as $categorias) {
                    $id_cat = $categorias->id;
                    $desc_cat = $id_cat . ' - ' . $categorias->nom_en . ' / ' . $categorias->nom_es;
                    if($id_cat == $macro_cat_id) {
                        $selected = 'selected="selected"';
                    } else {
                        $selected = '';
                    }
                ?>
                    <option value="<?php echo $id_cat; ?>" <?php echo $selected ?>><?php echo $desc_cat; ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        <!-- <img src='../../Colores/Multicolor 01/0101.png'> -->
        <div class="npt micro-color">
            <select id="micro_color__<?php echo $id_sap; ?>" name="color" class="form-control selectpicker" data-live-search="true" onchange="save_data(this)">
                <option value="">--- Elije un Color ---</option>
            <?php
            foreach($colors as $color) {
                $color_sap = $color->sap_color_id;
                $desc_color = $color_sap . ' - ' . $color->desc_esp . ' / ' . $color->desc_cat . ' / ' . $color->desc_ing;
                if($color_sap == $micro_color) {
                    $selected = 'selected="selected"';
                } else {
                    $selected = '';
                }
                $bolita = "../../Colores/$color->sap_color_id.png";
            ?>
                <option value="<?php echo $color_sap; ?>" <?php echo $selected ?> style="background-image:url('<?php echo $bolita ?>');background-repeat-x: no-repeat;background-repeat-y: no-repeat;"><?php echo $desc_color; ?></option>
            <?php
            }
            reset($colors);
            ?>
            </select>
        </div>
        
        <div class="npt descuento">
            <label for="rebajas__<?php echo $id_sap; ?>">Descuento %:</label>
            <input id="rebajas__<?php echo $id_sap; ?>" type="number" class="form-control" value="<?php echo $rebajas ?>" placeholder="Descuento %" min="0" max="100" value="0" onblur="save_data(this)">
        </div>
        
        <input id="brand__<?php echo $id_sap; ?>" value="<?php echo $data->nombre ?>" type="hidden" class="form-control" onblur="save_data(this)">
    </form>

<?php
    function get_on_off_line($name) {

        $db_my = new db('my','edescriptions');
        $marcas_offline = $db_my->make_query(queries::get_table_data('marcas_offline'));
        unset($db_my);

        $on_off_line = 'online';
        foreach($marcas_offline as $mo) {
            if(strpos($mo->name,$name)!=0) {
                $on_off_line = 'offline';
                break;
            }
        }
        return $on_off_line;
    }
?>