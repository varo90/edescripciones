<?php
    // include('db_connections.php');
    // include('queries.php');
    
    $my_conn = new db('my','edescriptions');
    
    $copia = 0;
    if(empty($item_desc)) {
        $item_description = $my_conn->make_query(queries::get_similar_item(),[$ref]);
        if(!empty($item_description)) {
            $item_description = $item_description[0];
            $copia = 1;
        } else {
            $item_description = null;
        }
    } else {
        $item_description = $my_conn->make_query(queries::get_item_description(),[$ref,$color])[0];
    }
    $colors = $my_conn->make_query(queries::get_table_data('colors'));
    $tallajes = $my_conn->make_query(queries::get_table_data('tallajes'));
    $categorias = $my_conn->make_query(queries::get_table_data('categorias'));
    
    unset($my_conn);

    $tit_esp = '';$tit_cat = '';$tit_eng = '';
    $desc_esp = '';$desc_cat = '';$desc_eng = '';
    $med_esp = '';$med_cat = '';$med_eng = '';
    $com_esp = '';$com_cat = '';$com_eng = '';
    $orientacion = '';$tallaje = '';$macro_cat_id = '';$sub_cat_id = '';
    $micro_color = '';$rebajas = '';
    if($item_description != null) {
        $tit_esp = $item_description->tit_esp;
        $tit_cat = $item_description->tit_cat;
        $tit_eng = $item_description->tit_eng;
        $desc_esp = $item_description->desc_esp;
        $desc_cat = $item_description->desc_cat;
        $desc_eng = $item_description->desc_eng;
        $med_esp = $item_description->med_esp;
        $med_cat = $item_description->med_cat;
        $med_eng = $item_description->med_eng;
        $com_esp = $item_description->com_esp;
        $com_cat = $item_description->com_cat;
        $com_eng = $item_description->com_eng;
        $orientacion = $item_description->orientacion;
        $tallaj = $item_description->tallaje;
        $macro_cat_id = $item_description->macro_cat_id;
        $sub_cat_id = $item_description->sub_cat_id;
        $micro_color = $item_description->micro_color;
        $rebajas = $item_description->rebajas;
    }
    
    
    $my_conn = new db('my','imagenes_ecommerce');
    
    $query = $my_conn->conn->prepare(queries::get_item_data());
    $query->execute([$ref,$color]);
    $data = $query->fetch(PDO::FETCH_OBJ);
    
    unset($my_conn);

    if($copia == 1) {
        $my_conn = new db('my','edescriptions');
        $query = $my_conn->conn->prepare(queries::clone_row());
        $query->execute([$ref,$item_description->color,$color]);
        unset($my_conn);
    }